# Google Tag Manager integration for Typo3

##What does it do?

This extension integrates the Google Tag Manager into your Typo3 website. 
 
The required script and noscript tags will be added automatically. All you have to do is configure your container id.  

Note that the tag manager integration is automatically disabled when you are logged into the backend or when the website is running in the development application context.

##Installation

- Install the extension via composer


    composer req hn/gtm   
    
- Include the static typoscript "Google Tag Manager" provided by this extension in your typoscript template.

- Set the google tag manager container id in your typoscript constants:


    plugin.tx_gtm.containerId = GTM-XXXX
    

Now you are ready to start using the Google Tag Manager in your Typo3 site.
 

