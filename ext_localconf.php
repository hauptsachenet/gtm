<?php

if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

if (TYPO3_MODE == 'FE') {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_pagerenderer.php']['render-postProcess'][] =
        \Hn\Gtm\Hook\PostProcessor::class . '->injectScriptTag';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['contentPostProc-all'][] =
        \Hn\Gtm\Hook\PostProcessor::class . '->injectNoscriptTag';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['insertPageIncache'][] =
        \Hn\Gtm\Service\DataLayerService::class;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['tslib/class.tslib_fe.php']['pageLoadedFromCache'][] =
        \Hn\Gtm\Service\DataLayerService::class . '->pageLoadedFromCache';
}

if (!is_array($GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][\Hn\Gtm\Service\DataLayerService::CACHE_CONFIGURATION_KEY])) {
    $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations'][\Hn\Gtm\Service\DataLayerService::CACHE_CONFIGURATION_KEY] = [
        'groups' => ['pages']
    ];
}