<?php

namespace Hn\Gtm\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\TimeTracker\TimeTracker;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class GoogleTagManagerService implements SingletonInterface
{
    const TYPOSCRIPT_OBJECT_PATH_HEADER_SCRIPT = 'lib.googleTagManagerScriptTag';
    const TYPOSCRIPT_OBJECT_PATH_BODY_NOSCRIPT = 'lib.googleTagManagerNoscriptTag';

    /**
     * @return string
     */
    public function renderHeaderSection()
    {
        return $this->renderCObject(self::TYPOSCRIPT_OBJECT_PATH_HEADER_SCRIPT);
    }

    /**
     * return string
     */
    public function renderBodySection()
    {
        return $noscriptTag = $this->renderCObject(self::TYPOSCRIPT_OBJECT_PATH_BODY_NOSCRIPT);
    }

    /**
     * @param string $typoscriptObjectPath
     * @return string
     */
    protected function renderCObject(string $typoscriptObjectPath)
    {
        $timeTracker = GeneralUtility::makeInstance(TimeTracker::class);
        $timeTracker->push('GoogleTagManagerServive->renderCObject', $typoscriptObjectPath);

        $pathSegments = GeneralUtility::trimExplode('.', $typoscriptObjectPath);
        $lastSegment = array_pop($pathSegments);

        $setup = $this->getConfigurationManager()->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        foreach ($pathSegments as $segment) {
            if (isset($setup[$segment . '.'])) {
                $setup = $setup[$segment . '.'];
            }
        }

        $result = self::getContentObjectRenderer()->cObjGetSingle($setup[$lastSegment], $setup[$lastSegment . '.']);

        $timeTracker->pull();

        return $result;
    }

    /**
     * @return ContentObjectRenderer
     */
    protected static function getContentObjectRenderer()
    {
        return GeneralUtility::makeInstance(
            ContentObjectRenderer::class,
            $GLOBALS['TSFE'] ?? GeneralUtility::makeInstance(TypoScriptFrontendController::class, null, 0, 0)
        );
    }

    /**
     * @return ConfigurationManagerInterface
     */
    protected function getConfigurationManager()
    {
        return GeneralUtility::makeInstance(ObjectManager::class)->get(ConfigurationManagerInterface::class);
    }
}