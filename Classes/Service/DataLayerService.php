<?php

namespace Hn\Gtm\Service;

use TYPO3\CMS\Core\Cache\CacheManager;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class DataLayerService implements SingletonInterface
{
    const CACHE_CONFIGURATION_KEY = 'gtm_datalayer_cache';

    /**
     * @var array
     */
    protected $dataLayer = [];

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->dataLayer;
    }

    /**
     * @param array $dataLayer
     */
    public function set(array $dataLayer): void
    {
        $this->dataLayer = $dataLayer;
    }

    /**
     * @param $data
     */
    public function add($data): void
    {
        $this->dataLayer = array_merge($this->dataLayer, $data);
    }

    /**
     * @param $params
     * @param TypoScriptFrontendController $typoScriptFrontendController
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     */
    public function pageLoadedFromCache(&$params, TypoScriptFrontendController $typoScriptFrontendController)
    {
        $this->dataLayer = GeneralUtility::makeInstance(CacheManager::class)->getCache(self::CACHE_CONFIGURATION_KEY)->get($typoScriptFrontendController->newHash) ?? [];

    }

    /**
     * @param TypoScriptFrontendController $typoScriptFrontendController
     * @param int $timeOutTime
     * @throws \TYPO3\CMS\Core\Cache\Exception\NoSuchCacheException
     */
    public function insertPageIncache(TypoScriptFrontendController $typoScriptFrontendController, int $timeOutTime)
    {
        $tags = ['pageId_' . $typoScriptFrontendController->id];

        GeneralUtility::makeInstance(CacheManager::class)->getCache(self::CACHE_CONFIGURATION_KEY)->set($typoScriptFrontendController->newHash, $this->dataLayer, $tags, $timeOutTime - $GLOBALS['EXEC_TIME']);
    }
}