<?php

namespace Hn\Gtm\UserFunc;

use Hn\Gtm\Service\DataLayerService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DataLayer
{
    /**
     * returns json encoded data layer
     *
     * @return false|string
     */
    public function renderDataLayerJson()
    {
        $data = $this->getDataLayerService()->get();

        if (empty($data)) {
            return '';
        }

        return json_encode($data);
    }
    /**
     * @return DataLayerService
     */
    protected function getDataLayerService()
    {
        return GeneralUtility::makeInstance(ObjectManager::class)->get(DataLayerService::class);
    }

}