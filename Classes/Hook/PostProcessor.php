<?php

namespace Hn\Gtm\Hook;

use Hn\Gtm\Service\GoogleTagManagerService;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

class PostProcessor
{
    /**
     * @param $params
     * @param PageRenderer $pageRenderer
     */
    public function injectScriptTag(&$params, PageRenderer $pageRenderer)
    {
        if ($scriptTag = GeneralUtility::makeInstance(ObjectManager::class)->get(GoogleTagManagerService::class)->renderHeaderSection()) {
            array_unshift(
                $params['headerData'],
                $scriptTag
            );
        }
    }

    /**
     * @param $params
     * @param TypoScriptFrontendController $tsfe
     */
    public function injectNoscriptTag(&$params, TypoScriptFrontendController $tsfe)
    {
        if ($noscriptTag = GeneralUtility::makeInstance(ObjectManager::class)->get(GoogleTagManagerService::class)->renderBodySection()) {
            $tsfe->content = preg_replace(
                '/<body[^>]*>/',
                '$0' . $noscriptTag,
                $tsfe->content,
                1
            );
        }
    }
}