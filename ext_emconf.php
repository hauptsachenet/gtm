<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Google Tag Manager integration for Typo3',
    'description' => 'Integrates the Google Tag Manager into your Typo3 website',
    'category' => 'frontend',
    'state' => 'excludeFromUpdates',
    'uploadfolder' => '0',
    'createDirs' => '',
    'clearCacheOnLoad' => true,
    'constraints' => [
        'depends' => [],
        'conflicts' => [],
        'suggests' => [],
    ],
    'author' => '',
    'author_email' => '',
    'author_company' => '',
];